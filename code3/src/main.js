/**
 * 游戏主函数
 */
import * as THREE from "../libs/three.js"
window.THREE = THREE
import game from "./game/game.js"
import * as TWEEN from "../libs/tween"
window.TWEEN = TWEEN
import * as CANNON from "../libs/cannon"
window.CANNON = CANNON
class Main {
    constructor() {
        
    }

    static init(){
        game.init()
        
    }
}


export default Main
