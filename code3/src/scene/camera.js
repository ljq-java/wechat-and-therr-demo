import sceneConf from "../../confs/scene-conf"
class Camera {
    constructor(){

    }
    init(){
        const aspect = window.innerHeight/window.innerWidth
        this.instance = new THREE.OrthographicCamera(-sceneConf.frustumSize, sceneConf.frustumSize, sceneConf.frustumSize * aspect, -sceneConf.frustumSize * aspect, -100, 85)
        // this.instance = new THREE.PerspectiveCamera(90, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.instance.position.set(-10, 10, 10)
        this.target = new THREE.Vector3(0, 0, 0)
        this.instance.lookAt(this.target)
        // this.instance.position.set(0,80,0)
        // this.instance.rotation.x = -Math.PI/2
        // this.instance.lookAt(new THREE.Vector3(0, 0, 0)) // 相机在0,0,0的位置往向 -10,10,10
    }
}

export default new Camera()
