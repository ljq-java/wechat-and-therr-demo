export default class GameOverPage{
    constructor(callbacks){
        this.callbacks = callbacks
    }
    init (options){
        this.initGameoverCanvas(options)
    }

    initGameoverCanvas(options){
        const aspect = window.innerHeight/window.innerWidth
        this.camera = options.camera
        this.canvas = document.createElement('canvas')
        this.canvas.width = window.innerWidth
        this.canvas.height = window.innerHeight
        this.texture = new THREE.Texture(this.canvas)
        this.material = new THREE.MeshBasicMaterial({
            map:this.texture,
            transparent:true,
            side:THREE.DoubleSide
        })
        this.geometry = new THREE.PlaneGeometry(this.canvas.width/2,this.canvas.height/2)
        this.obj = new THREE.Mesh(this.geometry,this.material)
        this.obj.position.z = 1
        // this.obj.rotation.y = Math.PI
        this.context = this.canvas.getContext('2d')
        this.context.fillStyle = "#333"
        this.context.fillRect((window.innerWidth-50)/2,(window.innerHeight-25)/2,50,25)
        this.context.fillStyle = "#eee"
        this.context.font = "20px Georgia"
        this.context.fillText("Start Game",(window.innerWidth-50)/2,(window.innerHeight-25)/2,50,25)
        this.texture.needsUpdate = true
        this.obj.visible = false
        this.camera.add(this.obj)

    }

    show(){
        console.log("game over page show")
        this.obj.visible =  true
    }

    hide(){
        this.obj.visible = false
    }
}