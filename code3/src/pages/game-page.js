import {
  scene,
  camera
} from "../scene/index.js"
import Cuboid from "../block/cuboid.js"
import Cylinder from "../block/cylinder.js"
import ground from "../objects/ground.js"
import bottle from "../objects/figure/bottle.js"
import bottle2 from "../objects/figure2/bottle2.js"
import {
  OrbitControls
} from '../../libs/OrbitControls'

import {
  GLTFLoader
} from '../../libs/GLTFLoader';

import {
  astar,
  Graph
} from '../../libs/astar.js';
import {
  Float32BufferAttribute,
  TorusGeometry
} from "../../libs/three.js"

export default class GamePage {
  constructor(callbacks) {
    this.callbacks = callbacks
  }

  init() {
    this.scene = scene
    this.camera = camera // 初始化获取相机
    this.world = new CANNON.World() // 世界
    this.world.broadphase = new CANNON.SAPBroadphase(this.world)
    this.world.allowSleep = true
    this.world.gravity.set(0, -9.82, 0) // 世界重力为 9.82
    this.clock = new THREE.Clock()
    this.touchStart = "";
    this.objectsToUpdate = [];
    this.ground = ground // 地面
    this.bottle = bottle // 瓶子
    this.bottle2 = bottle2 // 瓶子
    this.grapha = []
    this.scene.init()
    this.ground.init()
    // this.bottle.init()
    // this.bottle2.init()
    this.mapObjs = []
    this.objList = [];
    this.addInitBlock()
    this.addGround()
    this.Asuanfa()
    // this.addBottle()
    // this.addBottle2() 
    this.wulicaizhi() // 添加物理材质
    this.addWorldSphere() // 添加球体
    this.addWoeldCylinder() // 添加圆柱
    this.addWorldCone() // 添加菱锥
    this.addWorldBox() // 添加正方体
    this.addPlaneCA() // 地面
    // this.addLifangti() // 添加立方体
    this.addxlr() // 添加小绿人
    this.controls = new OrbitControls(this.camera.instance, this.scene.renderer.domElement);
    this.bindTouchEvent() // 点击屏幕
    this.worldAddFense() // 篱笆围墙
    this.glbchangjing() // 场景加载
    this.render()
  }

  render() {
    var elapsedTime = this.clock.getElapsedTime()
    var deltaTime = elapsedTime - this.lodElapsedTime
    this.lodElapsedTime = elapsedTime

    this.scene.render()

    // Update physics world
    // this.sphereBody.applyForce(new CANNON.Vec3(0.5,0,0),this.sphereBody.position)
    this.world.step(1 / 60, deltaTime, 3)

    // this.sphere.position.copy(this.sphereBody.position)
    for (const object of this.objectsToUpdate) {
      object.mesh.position.copy(object.body.position)
      object.mesh.quaternion.copy(object.body.quaternion)
    }

    TWEEN.update();
    requestAnimationFrame(this.render.bind(this))
  }

  // 场景加载
  glbchangjing(){

  }
  // 添加小绿人
  addxlr() {
    // let file = 'res/glb/testwl1.glb';
    // let fileText = 'res/images/uvtest.jpg';
    // let file = 'res/glb/portal.glb';
    // let fileText = 'res/images/baked.jpg';
    let file = 'res/glb/cjwl10.glb';
    let fileText = 'res/images/cjwl10.jpg';
    const fs = wx.getFileSystemManager();
    let content = fs.readFileSync(file);
    const loaderText = new THREE.TextureLoader()
    var texturesList  = loaderText.load(fileText)
    texturesList.flipY = false
    let loader = new GLTFLoader();
    let that = this
    const backedMaterial = new THREE.MeshBasicMaterial({ map:texturesList })
    // const backedMaterial = new THREE.MeshBasicMaterial({ color:0xff0000 })
    loader.parse(content, file, function (gltf) {
      gltf.scene.traverse((children)=>{
        children.material = backedMaterial
      })
      that.xiaolvren = gltf.scene
      
      // console.log(gltf.scene,"----====")
      that.xiaolvren.scale.set(10, 10, 10);
      that.xiaolvren.rotation.y = Math.PI
      that.xiaolvren.position.x = 0
      that.xiaolvren.position.y = 10
      that.xiaolvren.position.z = 0
      that.xiaolvren.visible = true;
      that.scene.instance.add(that.xiaolvren)
    })
  }

  // 篱笆 围墙
  worldAddFense() {
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 2
    }) // A 蓝色 x正
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 6
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 10
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 14
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 18
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 22
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 26
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 30
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 34
    })

    this.createWhiteBox(4, 4, 4, {
      x: 26,
      y: 4,
      z: 34
    }) //
    this.createWhiteBox(4, 4, 4, {
      x: 22,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 18,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 14,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 10,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 6,
      y: 4,
      z: 34
    })

    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 38
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 42
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 46
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 50
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 54
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 58
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 62
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 70
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 74
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 78
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 82
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 86
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 90
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 94
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: 98
    }) //

    this.createWhiteBox(4, 4, 4, {
      x: 26,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: 22,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: 18,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: 14,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: 10,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: 6,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: 2,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -2,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -6,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -10,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -14,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -18,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -22,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -26,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -30,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 98
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 98
    }) //

    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 94
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 90
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 86
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 82
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 78
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 74
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 70
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 66
    }) //

    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 62
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 58
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 54
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 50
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 46
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 42
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 38
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 34
    }) //
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -30,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -26,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -22,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -18,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -14,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -10,
      y: 4,
      z: 34
    })

    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 30
    }) //
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 26
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 22
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 18
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 14
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: 10
    }) //

    this.createWhiteBox(4, 4, 4, {
      x: -42,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -46,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -50,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -54,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -58,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -62,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -66,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -70,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -74,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -78,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -82,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -86,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -90,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -94,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -98,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 62
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 58
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 54
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 50
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 46
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 42
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 38
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 30
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 26
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 22
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 18
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 14
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 10
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 6
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: 2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -2
    })

    this.createWhiteBox(4, 4, 4, {
      x: -98,
      y: 4,
      z: -2
    }) //
    this.createWhiteBox(4, 4, 4, {
      x: -94,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -90,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -86,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -82,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -78,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -74,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -70,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -66,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -62,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -58,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -54,
      y: 4,
      z: -2
    })
    this.createWhiteBox(4, 4, 4, {
      x: -50,
      y: 4,
      z: -2
    })

    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -6
    }) //
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -10
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -14
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -18
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -22
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -26
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -30
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -38
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -42
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -46
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -50
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -54
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -58
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -62
    })
    this.createWhiteBox(4, 4, 4, {
      x: -102,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -98,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -94,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -90,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -86,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -82,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -78,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -74,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -70,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -66,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -62,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -58,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -54,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -50,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -46,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -42,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -38,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -66
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -62
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -58
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -54
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -50
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -46
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -42
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -38
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -30
    }) // 

    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -26
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -22
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -18
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -14
    })
    this.createWhiteBox(4, 4, 4, {
      x: -34,
      y: 4,
      z: -10
    })

    this.createWhiteBox(4, 4, 4, {
      x: -30,
      y: 4,
      z: -34
    }) //
    this.createWhiteBox(4, 4, 4, {
      x: -26,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -22,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -18,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -14,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -10,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -6,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: -2,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 2,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 6,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 10,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 14,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 18,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 22,
      y: 4,
      z: -34
    })
    this.createWhiteBox(4, 4, 4, {
      x: 26,
      y: 4,
      z: -34
    })

    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -34
    }) // 
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -30
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -26
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -22
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -18
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -14
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -10
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -6
    })
    this.createWhiteBox(4, 4, 4, {
      x: 30,
      y: 4,
      z: -2
    })

  }

  // A* 自动寻路机器算法 添加路障
  Asuanfa() {
    this.maps = new Graph(this.grapha);
    // var startX = parseInt(2 / ws);
    // var startY = parseInt(2 / ws);
    // var endX = parseInt(4 / ws);
    // var endY = parseInt(4 / ws);
    var start = this.maps.grid[0][0];
    var end = this.maps.grid[15][15];
    var result = astar.search(this.maps, start, end);
    // 给路线分段
    var aa = [0, 0, 0, 0, 500]
    this.section = []

    for (let i = 0; i < result.length; i++) {
      if (aa[0] === result[i].x) {
        aa[2] = result[i].x
        aa[3] = result[i].y
        aa[4] = Number(aa[4]) + 500
      } else if (aa[1] === result[i].y) {
        aa[2] = result[i].x
        aa[3] = result[i].y
        aa[4] = Number(aa[4]) + 500
      } else {
        this.section.push(aa)
        var ff;
        if (i === aa.length - 1) {
          ff = [result[i - 1].x, result[i - 1].y, result[i].x, result[i].y, 500]
        } else {
          ff = [result[i - 1].x, result[i - 1].y, result[i].x, result[i].y, 1000]
        }
        aa = ff
      }
    }
    this.section.push(aa)
  }

  // Sphere
  createSphere(radius, position) {
    const mesh1 = new THREE.Mesh(
      new THREE.SphereBufferGeometry(radius, 20, 30),
      new THREE.MeshStandardMaterial({
        color: 0xff0000,
        metalness: 0.3,
        roughness: 0.4,
        // envMap: environmentMapTexture
      })
    )
    var mesh = mesh1.clone()
    mesh.castShadow = true
    mesh.position.copy(position)
    this.scene.instance.add(mesh)

    const shape = new CANNON.Sphere(radius)
    const body = new CANNON.Body({
      mass: 1,
      position: new CANNON.Vec3(0, 3, 0),
      shape,
      material: this.defaultMaterial
    })
    body.position.copy(position)
    this.world.addBody(body)

    // Save in objects to update
    this.objectsToUpdate.push({
      mesh,
      body
    })
  }

  // 添加物理材质
  wulicaizhi() {
    this.defaultMaterial = new CANNON.Material('default')

    var defaultContactMaterial = new CANNON.ContactMaterial(
      this.defaultMaterial,
      this.defaultMaterial, {
        friction: 0.2,
        restitution: 0.7
      }
    )
    this.world.addContactMaterial(defaultContactMaterial)
    this.world.defaultContactMaterial = defaultContactMaterial
  }

  // 世界添加地面
  addPlaneCA() {
    var floorShape = new CANNON.Plane()
    var floorBody = new CANNON.Body()
    floorBody.name = "地面"
    floorBody.mass = 0
    floorBody.addShape(floorShape)
    floorBody.quaternion.setFromAxisAngle(
      new CANNON.Vec3(-1, 0, 0),
      Math.PI * 0.5
    )
    this.world.addBody(floorBody)
  }

  // 世界添加球体
  addWorldSphere() {
    this.createSphere(2, {
      x: 4,
      y: 8,
      z: 0
    })
    this.createSphere(2, {
      x: 0,
      y: 8,
      z: 0
    })
    this.createSphere(2, {
      x: -4,
      y: 8,
      z: 0
    })
    console.log(this.objectsToUpdate)
  }

  // 创建立方体 
  createBox(width, height, depth, position) {
    const mesh1 = new THREE.Mesh(
      new THREE.BoxBufferGeometry(width, height, depth),
      new THREE.MeshStandardMaterial({
        color: 0xff0000,
        metalness: 0.3,
        roughness: 0.4,
        // envMap: environmentMapTexture
      })
    )
    var mesh = mesh1.clone()
    mesh.castShadow = true
    mesh.position.copy(position)
    this.scene.instance.add(mesh)

    const shape = new CANNON.Box(new CANNON.Vec3(width, height, depth))
    const body = new CANNON.Body({
      mass: 1,
      shape,
      material: this.defaultMaterial
    })
    body.position.copy(position)
    this.world.addBody(body)

    // Save in objects to update
    this.objectsToUpdate.push({
      mesh,
      body
    })
  }

  // 创建白色立方体
  createWhiteBox(width, height, depth, position) {
    const mesh1 = new THREE.Mesh(
      new THREE.BoxBufferGeometry(width, height, depth),
      new THREE.MeshStandardMaterial({
        color: 0xffffff,
      })
    )
    var mesh = mesh1.clone()
    // mesh.castShadow = true
    mesh.position.copy(position)
    this.scene.instance.add(mesh)
  }

  // 添加圆柱
  addWoeldCylinder() {
    this.createCylinder(2,2,2,{
      x: -56,
      y: 2,
      z: -30
    })
    this.createCylinder(2,2,2,{
      x: -60,
      y: 2,
      z: -30
    })
    this.createCylinder(2,2,2,{
      x: -64,
      y: 2,
      z: -30
    })
  }

  // 创建圆柱
  createCylinder(radiusTop,radiusBottom,height,position) {
    // 创建一个圆柱体几何体
    var geometry = new THREE.CylinderGeometry(radiusTop, radiusBottom, height,8);

    // 创建一个材质
    var material = new THREE.MeshBasicMaterial({
      color: 0xff0000
    });

    // 创建圆柱体网格对象
    var mesh1 = new THREE.Mesh(geometry, material);
    var mesh = mesh1.clone()
    mesh.position.copy(position)
    this.scene.instance.add(mesh)

    // 创建 Cannon.js 中的圆柱体刚体
    var shape = new CANNON.Cylinder(radiusTop, radiusBottom, height,8);
    var body = new CANNON.Body({
      mass: 1
    });
    body.addShape(shape);
    // 设置刚体的位置和旋转等属性

    // 将刚体添加到 Cannon.js 的世界中
    body.position.copy(position)
    this.world.addBody(body)

    // Save in objects to update
    this.objectsToUpdate.push({
      mesh,
      body
    })

  }

  // 添加棱锥
  addWorldCone() {
    this.createCone(2, 2, {
      x: 4,
      y: 3,
      z: 60
    })
    this.createCone(2, 2, {
      x: 0,
      y: 3,
      z: 60
    })
    this.createCone(2, 2, {
      x: -4,
      y: 3,
      z: 60
    })
  }

  // 创建 棱锥
  createCone(radius, height, position) {
    // 创建一个棱锥的几何体
    var geometry = new THREE.ConeGeometry(radius, height, 8);

    // 创建一个材质
    var material = new THREE.MeshBasicMaterial({
      color: 0xff0000
    });

    // 创建棱锥的网格对象
    var mesh1 = new THREE.Mesh(geometry, material);

    var mesh = mesh1.clone()
    mesh.position.copy(position)
    this.scene.instance.add(mesh)

    // 创建 Cannon.js 中的刚体
    var shape = new CANNON.Cylinder(0, radius, height, 4);
    var body = new CANNON.Body({
      mass: 1
    });
    body.addShape(shape);
    // 设置刚体的位置和旋转等属性

    // 将刚体添加到 Cannon.js 的世界中
    body.position.copy(position)
    this.world.addBody(body)

    // Save in objects to update
    this.objectsToUpdate.push({
      mesh,
      body
    })
  }

  // 世界添加正方体
  addWorldBox() {
    this.createBox(4, 4, 4, {
      x: -64,
      y: 4,
      z: 32
    })
    this.createBox(4, 4, 4, {
      x: -56,
      y: 4,
      z: 32
    })
    this.createBox(4, 4, 4, {
      x: -72,
      y: 4,
      z: 32
    })
  }

  // 添加地块
  addInitBlock() {
    // var cuboidBlock = new Cuboid(0, 0, 0)
    // cuboidBlock = cuboidBlock.instance.clone()
    // this.scene.instance.add(cuboidBlock.instance)

    const geometry = new THREE.BoxGeometry(3.8, 1, 3.8)
    const material = new THREE.MeshPhongMaterial({
      color: 0x3380E6 // #E9905E  ffffff
    })
    var meshObj = new THREE.Mesh(geometry, material)
    var z1 = 0
    var x1 = 0
    for (let z = -32; z < 32; z++) {
      var num = []
      var numobj = []
      for (let x = -32; x < 32; x++) {
        if (x % 4 == 0 && z % 4 == 0) {
          num.push(1)
          numobj.push([z, x])
          let meshObj2 = meshObj.clone()
          meshObj2.position.x = x
          meshObj2.position.z = z
          this.scene.instance.add(meshObj2)
        }
        x1++
      }
      if (z % 4 == 0) {
        this.grapha.push(num)
        this.mapObjs.push(numobj)
      }
      z1++
    }
    z1 = 0
    x1 = 0
    const geometry1 = new THREE.BoxGeometry(3.8, 1, 3.8)
    const material1 = new THREE.MeshPhongMaterial({
      color: 0x4A9C53 // #4C9D54  ffffff
    })
    var meshObj1 = new THREE.Mesh(geometry1, material1)
    for (let z = -64; z < 0; z++) {
      var num = []
      var numobj = []
      for (let x = -96; x < -32; x++) {
        if (x % 4 == 0 && z % 4 == 0) {
          num.push(1)
          numobj.push([z, x])
          let meshObj3 = meshObj1.clone()
          meshObj3.position.x = x
          meshObj3.position.z = z
          this.scene.instance.add(meshObj3)
        }
        x1++
      }
      if (z % 4 == 0) {
        this.grapha.push(num)
        this.mapObjs.push(numobj)
      }
      z1++
    }
    z1 = 0
    x1 = 0
    const geometry2 = new THREE.BoxGeometry(3.8, 1, 3.8)
    const material2 = new THREE.MeshPhongMaterial({
      color: 0xF6BF44 // #F6BF44
    })
    var meshObj2 = new THREE.Mesh(geometry2, material2)
    for (let z = 0; z < 64; z++) {
      var num = []
      var numobj = []
      for (let x = -96; x < -32; x++) {
        if (x % 4 == 0 && z % 4 == 0) {
          num.push(1)
          numobj.push([z, x])
          let meshObj3 = meshObj2.clone()
          meshObj3.position.x = x
          meshObj3.position.z = z
          this.scene.instance.add(meshObj3)
        }
        x1++
      }
      if (z % 4 == 0) {
        this.grapha.push(num)
        this.mapObjs.push(numobj)
      }
      z1++
    }

    z1 = 0
    x1 = 0
    const geometry3 = new THREE.BoxGeometry(3.8, 1, 3.8)
    const material3 = new THREE.MeshPhongMaterial({
      color: 0xDF5541 // #DF5541
    })
    var meshObj3 = new THREE.Mesh(geometry3, material3)
    for (let z = 32; z < 96; z++) {
      var num = []
      var numobj = []
      for (let x = -32; x < 32; x++) {
        if (x % 4 == 0 && z % 4 == 0) {
          num.push(1)
          numobj.push([z, x])
          let meshObj = meshObj3.clone()
          meshObj.position.x = x
          meshObj.position.z = z
          this.scene.instance.add(meshObj)
        }
        x1++
      }
      if (z % 4 == 0) {
        this.grapha.push(num)
        this.mapObjs.push(numobj)
      }
      z1++
    }
  }

  // 平移
  translate(obj, targetPosition, time) {
    return new Promise((resolve, reject) => {
      const tween = new TWEEN.Tween(obj.position)
        .to(targetPosition, time)
        // .onUpdate(() => {
        // })
        .onComplete(() => {
          resolve("0")
        })
        .start();
    })
  }

  // 添加地面
  addGround() {
    this.scene.instance.add(this.ground.instance)
    this.bottle.showup()
    this.bottle2.showup()
  }


  // 添加瓶子
  addBottle() {
    this.scene.instance.add(this.bottle.obj)
  }
  // 添加瓶子2
  addBottle2() {
    this.scene.instance.add(this.bottle2.obj)
  }

  show() {
    this.mesh.visible = true
  }

  hide() {
    this.mesh.visible = false
  }

  restart() {
    console.log("game page restart")
  }

  onTouchEnd(e) {
    // console.log(e, "--结束--")
    // const pageEndX = e.changedTouches[0].pageX
    // const pageEndZ = e.changedTouches[0].pageY

    // console.log(this.touchStart, "--开始--")
    // const pageStartX = this.touchStart.pageX
    // const pageStartZ = this.touchStart.pageY

    // var position = this.camera.instance.position

    // var pageX = pageEndX - pageStartX
    // var pageZ = pageEndZ - pageStartZ
    // //x>0  地图往右
    // if (pageX > 0) {
    //   // 地图往右
    //   //   position.x = position.x - 10
    //   // position.z = position.z - 10
    //   var targetPosition = {
    //     x: position.x - 10,
    //     z: position.z - 10
    //   };
    //   this.translate(this.camera.instance, targetPosition, 500).then((res) => {
    //     if (pageZ < 0) {
    //       // 地图往上
    //       // position.x = position.x - 10
    //       // position.z = position.z + 10
    //       var targetPosition = {
    //         x: position.x - 10,
    //         z: position.z + 10
    //       };
    //       this.translate(this.camera.instance, targetPosition, 500)

    //     } else if (pageZ > 0) {
    //       // 地图往下
    //       // position.x = position.x + 10
    //       // position.z = position.z - 10
    //       var targetPosition = {
    //         x: position.x + 10,
    //         z: position.z - 10
    //       };
    //       this.translate(this.camera.instance, targetPosition, 500)
    //     }
    //   })
    // } else if (pageX < 0) { //x<0  地图往左
    //   // 地图往左
    //   // position.x = position.x + 10
    //   // position.z = position.z + 10
    //   var targetPosition = {
    //     x: position.x + 10,
    //     z: position.z + 10
    //   };
    //   this.translate(this.camera.instance, targetPosition, 500).then((res) => {
    //     if (pageZ < 0) {
    //       // 地图往上
    //       // position.x = position.x - 10
    //       // position.z = position.z + 10
    //       var targetPosition = {
    //         x: position.x - 10,
    //         z: position.z + 10
    //       };
    //       this.translate(this.camera.instance, targetPosition, 500)

    //     } else if (pageZ > 0) {
    //       // 地图往下
    //       // position.x = position.x + 10
    //       // position.z = position.z - 10
    //       var targetPosition = {
    //         x: position.x + 10,
    //         z: position.z - 10
    //       };
    //       this.translate(this.camera.instance, targetPosition, 500)
    //     }
    //   })

    // }


    // this.camera.instance.position.set(position.x, position.y, position.z)

  }

  // 开始
  onTouchstart(e) {
    const pageX = e.changedTouches[0].pageX
    const pageY = e.changedTouches[0].pageY
    if (this.touchStart == "") {
      this.touchStart = e.changedTouches[0]
    }
  }

  // 结束
  bindTouchEvent() {
    this.onTouchstart = this.onTouchstart.bind(this)
    this.onTouchEnd = this.onTouchEnd.bind(this)
    canvas.addEventListener('touchstart', this.onTouchstart)
    canvas.addEventListener('touchend', this.onTouchEnd)
  }

  removeTouchEvent() {
    canvas.removeEventListener('touchend', this.onTouchEnd)
  }

}